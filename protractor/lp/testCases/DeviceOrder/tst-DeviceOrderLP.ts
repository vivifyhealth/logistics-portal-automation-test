import { browser, element, by } from 'protractor'
import { loginpage } from '../../pages/common/loginpage';
import { Vconst } from '../../util/Vconst';
import { LPTopNavigation } from '../../pages/common/LPTopNavigation';
import { fulfillmentOrderList } from '../../pages/Logistics/fulfillmentOrders/fulfillmentOrderList';
import { fulfillmentCreateDeviceOrderWizard } from '../../pages/Logistics/fulfillmentOrders/fulfillmentCreateDeviceOrderWizard';
const protractor_1 = require("protractor");


let login = new loginpage();
let lpTopNavigation = new LPTopNavigation();
let lpFulfillmentOrderList = new fulfillmentOrderList();
let lpFulfillmentCreateDeviceOrderWizard = new fulfillmentCreateDeviceOrderWizard();

let customerName: string = "VH - Inventory";
let manufacturerName: string = "Hypertec";

// create device order from logistics portal
describe('Device Order - Create Device Order from LP TST-DeviceOrder', function () {

    it('STEP 1: Login as a LP user and navigate to fulfillments list', async () => {

        console.log(" TST-DeviceOrder start");

        await browser.get(Vconst.url);
        browser.driver.manage().window().maximize();
        login.login(Vconst.D_username, Vconst.D_password);
        expect(browser.getCurrentUrl()).toContain('/Dashboard');

        lpTopNavigation.clickOnLogisticsMenuLink();
        lpTopNavigation.clickOnFulfillmentOrdersMenuLink();
        browser.manage().timeouts().implicitlyWait(Vconst.lwaitTime);
        lpFulfillmentOrderList.clickOnActionsMenuLink();
        lpFulfillmentOrderList.clickOnCreateDeviceOrderMenuLink();

    });

    it('STEP 2: select customer and manufacturer', async () => {

        console.log(" TST-DeviceOrder STEP 2: select customer and manufacturer");
        lpFulfillmentCreateDeviceOrderWizard.clickOnCustomerListForCustomer(customerName);
        lpFulfillmentCreateDeviceOrderWizard.clickOnManufacturer(manufacturerName);
        lpFulfillmentCreateDeviceOrderWizard.verifyCustomerNextButton();
        lpFulfillmentCreateDeviceOrderWizard.clickCustomerNextButton();     
        browser.manage().timeouts().implicitlyWait(Vconst.mwaitTime);
    });

    it('STEP 3: add devices', async () => {

        console.log(" TST-DeviceOrder STEP 3: add devices");
        lpFulfillmentCreateDeviceOrderWizard.clickAddDeviceButton();
        lpFulfillmentCreateDeviceOrderWizard.setFirstDeviceQuantity('1');
        lpFulfillmentCreateDeviceOrderWizard.clickOnFirstDeviceType('Scale');
        lpFulfillmentCreateDeviceOrderWizard.setFirstDeviceModel('AnD UC-321PBT Scale');
        lpFulfillmentCreateDeviceOrderWizard.verifyAddDevicesNextButton();
        lpFulfillmentCreateDeviceOrderWizard.clickAddDevicesNextButton();
        browser.manage().timeouts().implicitlyWait(Vconst.mwaitTime);
    });

    it('STEP 4: shipping information', async () => {
        console.log(" TST-DeviceOrder STEP 4: shipping information");
        lpFulfillmentCreateDeviceOrderWizard.setFirstName('Jane');
        lpFulfillmentCreateDeviceOrderWizard.setLastName('Tester');
        lpFulfillmentCreateDeviceOrderWizard.setAddressLine1('7201 Bishop Rd');
        lpFulfillmentCreateDeviceOrderWizard.setAddressLine2('Suite 200');
        lpFulfillmentCreateDeviceOrderWizard.setCity('Plano');
        lpFulfillmentCreateDeviceOrderWizard.setState('TX');
        lpFulfillmentCreateDeviceOrderWizard.setZipCode('75024');
        lpFulfillmentCreateDeviceOrderWizard.setPhone1('469-555-0000');
        lpFulfillmentCreateDeviceOrderWizard.verifyShippingNextButton();
        lpFulfillmentCreateDeviceOrderWizard.clickShippingNextButton();

        browser.manage().timeouts().implicitlyWait(Vconst.mwaitTime);
    });

     it('STEP 5: confirmation page', async () => {
        console.log(" TST-DeviceOrder STEP 4: shipping information");
        lpFulfillmentCreateDeviceOrderWizard.verifyConfirmButton();
         lpFulfillmentCreateDeviceOrderWizard.clickConfirmButton();
        browser.manage().timeouts().implicitlyWait(Vconst.mwaitTime);
    });

    it("Post-condition", async () => {


    });

});