import { browser, element, by } from 'protractor'
import { loginpage } from '../../pages/common/loginpage';
import { Vconst } from '../../util/Vconst';
import { LPTopNavigation } from '../../pages/common/LPTopNavigation';
import { deviceProductOrder } from '../../pages/Devices/PO/deviceProductOrderPage';
import { addNewDevicePoPage } from '../../pages/Devices/PO/addNewDevicePOPage';
import { PoDetailPage } from '../../pages/Devices/PO/PoDetailPage';

const protractor_1 = require("protractor");

let login = new loginpage();
let lpTopNavigation = new LPTopNavigation();
let devicePO = new deviceProductOrder();
let newPO = new addNewDevicePoPage();
let PoDetails = new PoDetailPage();

let condition: string = "New";
let LogisticsCenter: string = "Vivify Development";
let owner: string = "VH - Inventory";
let summary: string = "Automation Create Device PO";
let description: string = "Automated Device PO Creation";
let note:string = "Automation Note"

//Create New Device Product Order
describe('Create a new Device Product Order', function () {

    it('STEP 1: Login as a LP user and navigate to Devices Product Order', async () => {

        console.log("Login to LP");
        await browser.get(Vconst.url);
        browser.driver.manage().window().maximize();
        login.login(Vconst.D_username, Vconst.D_password);
        expect(browser.getCurrentUrl()).toContain('/Dashboard');

        lpTopNavigation.clickOnDevicesTopLink();
        lpTopNavigation.clickOnDevicesPO();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    });

    it('STEP 2: Add a new Device PO', async () => {

        console.log("Add PO");
        devicePO.clickActionsButtonLink();
        devicePO.clickAddPO();

        expect(browser.getCurrentUrl()).toContain('/InventoryPO/New');
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);        
    });

    it('STEP 3: Enter PO Information', async () => {

        console.log("Enter PO Information");
        newPO.setSummary('Automation Create Device PO');
        newPO.setVendor('Hypertec');
        newPO.clickLogisticsLocation(LogisticsCenter);
        newPO.setReferencePO('159248');
        newPO.clickCondition(condition);
        newPO.clickOwner(owner);
        newPO.setDescription('Automated Device PO Creation');
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    });

    it('STEP 4: Add Devices', async () => {

        console.log("Add tablets to the order");
        newPO.clickAddDevice();
        newPO.setFirstDeviceQuantity('10');
        newPO.setFirstDeviceType('Tablet');
        newPO.setFirstDeviceModel('Verizon Tab A (8 in 32 GB)');
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    });

    it('STEP 5: Add Parts', async () => {

        console.log("Add USB Cables to the order");
        newPO.clickAddPart();
        newPO.setFirstPartQuantity('10');
        newPO.setFirstPartType('USB Cable');
        newPO.setFirstPartModel('Tablet USB Cable');
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    });

    it('STEP 6: Save the PO', async () => {

        console.log("Save the Order");
        newPO.clickSave();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    });

    it('STEP 7: Verify Success Banner', async () => {

        console.log("Verify the banner");
        PoDetails.verifySuccess();
        PoDetails.closeSucessBanner();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    });

    it('STEP 8: Verify Actions Menu', async () => {

        console.log("Verify Actions Dropdown");
        PoDetails.openActionsMenu();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        expect(PoDetails.editPO.getText()).toBe("Edit");
        console.log("Edit is present")
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        expect(PoDetails.completePO.getText()).toBe("Mark Complete");
        console.log("Complete is present")
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        expect(PoDetails.deletePO.getText()).toBe("Delete Order");
        console.log("Delete is present")
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        PoDetails.closeActionsMenu();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    });

    it('STEP 9: Verify History', async () => {

        console.log("Verify history section");
        PoDetails.historyText.getText().then(function(historyText){
            expect(historyText).toBe("Purchase order created");
        });
    });

    it('STEP 10: Verify Adding Notes', async () => {

        console.log("Verify notes section");
        PoDetails.clickAddNote();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        PoDetails.enterNote(note);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        PoDetails.clickSaveNote();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        PoDetails.savedNote.getText().then(function(noteText){
            expect(noteText).toBe(note);
        });
    });



    it('STEP 11: Verify PO Details', async () =>  {
        
        console.log("Verify the PO Details are correct");
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    
        PoDetails.PoSummary.getText().then(function(detailSummary){
            console.log(detailSummary);
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(detailSummary).toContain(summary);
        });    

         PoDetails.details1.getText().then(function(detailColumn1) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(detailColumn1).toContain('Hypertec', '159248');
        });

        PoDetails.details2.getText().then(function(detailColumn2) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(detailColumn2).toContain(owner);
            expect(detailColumn2).toContain(condition);
            expect(detailColumn2).toContain(LogisticsCenter);
        });

        PoDetails.PoDescription.getText().then(function(PoDescription) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(PoDescription).toContain(description);
        });

        PoDetails.firstDevicesOrdered.getText().then(function(quantityOrdered) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(quantityOrdered).toContain("10");
        });

        PoDetails.firstDevicesReceived.getText().then(function(quantityReceived) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(quantityReceived).toContain("0");
        });

        PoDetails.firstDevicesType.getText().then(function(type) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(type).toContain("Tablet");
        });

        PoDetails.firstDevicesModel.getText().then(function(model) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(model).toContain("Verizon Tab A");
        });

        PoDetails.firstDevicesPartNum.getText().then(function(partNum) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(partNum).toContain("SM-T387VZKAVZW");
        });

        PoDetails.secondDevicesOrdered.getText().then(function(quantityOrdered) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(quantityOrdered).toContain("10");
        });

        PoDetails.secondDevicesReceived.getText().then(function(quantityReceived) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(quantityReceived).toContain("0");
        });

        PoDetails.secondDevicesType.getText().then(function(type) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(type).toContain("USB Cable");
        });

        PoDetails.secondDevicesModel.getText().then(function(model) {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            expect(model).toContain("Tablet USB Cable");
        });

    });

    it('Post-condition', async () => {

        console.log("Delete the order")
        PoDetails.openActionsMenu();
        PoDetails.deletePO.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    });

});
