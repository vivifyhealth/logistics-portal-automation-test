import { browser, element, by } from 'protractor'
import { loginpage } from '../../pages/common/loginpage';
import { Vconst } from '../../util/Vconst';
import { LPTopNavigation } from '../../pages/common/LPTopNavigation';
import { deviceProductOrder } from '../../pages/Devices/PO/deviceProductOrderPage';
import { addNewDevicePoPage } from '../../pages/Devices/PO/addNewDevicePOPage';
import { PoDetailPage } from '../../pages/Devices/PO/PoDetailPage';


const protractor_1 = require("protractor");




let login = new loginpage();
let lpTopNavigation = new LPTopNavigation();
let devicePO = new deviceProductOrder();
let newPO = new addNewDevicePoPage();
let PoDetails = new PoDetailPage();


let condition: string = "New";
let LogisticsCenter: string = "Vivify Development";
let owner: string = "VH - Inventory";


//Create New Device Product Order
describe('Saving the  PO Number', function () {

    it('Navigate to the Devices page', async () => {

        console.log("Login to LP");
        await browser.get(Vconst.url);
        browser.driver.manage().window().maximize();
        login.login(Vconst.D_username, Vconst.D_password);
        expect(browser.getCurrentUrl()).toContain('/Dashboard');

        lpTopNavigation.clickOnDevicesTopLink();
        lpTopNavigation.clickOnDevicesPO();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    });

    it('Save the PO Number', async () => {
        
        devicePO.firstPoNumber.getText().then(function(PoId){
            console.log(PoId);

        });
    });

    /*
    it('Open the first PO', async () => {
        devicePO.openFirstPo();
        browser.manage().timeouts().implicitlyWait(Vconst.mwaitTime);
    });
*/    
});