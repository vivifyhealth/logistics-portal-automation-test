"use strict";

import { browser } from "protractor";
var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

//exports.__esModule = true;
// An example configuration file
exports.config = {
    // The address of a running selenium server.
    //seleniumAddress: 'http://localhost:4444/wd/hub',
    directConnect: true,
    framework: 'jasmine2',
    allScriptsTimeout: 50000,
    // Capabilities to be passed to the webdriver instance.
    capabilities: {
        browserName: 'chrome',
        'chromeOptions': {
            'excludeSwitches': ['enable-automation']
        }
    },

    // exclude: ['../TST-3358.js'],
    specs: ['../createDevicePO.js'],

    // Options to be passed to Jasmine-node.
    jasmineNodeOpts: {
        defaultTimeoutInterval: 300000,
        onComplete: null,
        isVerbose: true,
        showColors: true,
        includeStackTrace: true
    },

    onPrepare: function () {
        browser.driver.manage().window().maximize();
        require("jasmine-expect");

        //lp we are using against a non-angular application.
        browser.waitForAngularEnabled(false);
        //For Screen Shots 
        jasmine.getEnv().addReporter(
            new Jasmine2HtmlReporter({
                savePath: 'target/screenshots',
                takeScreenshots: false,
                takeScreenshotsOnlyOnFailures: false

            })
        );
    },

    suites:
    {

        Smoke: ['../*.js']

    }
};
