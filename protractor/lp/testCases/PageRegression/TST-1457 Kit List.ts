import { browser, element, by } from 'protractor'
import { loginpage } from '../../pages/common/loginpage';
import { Vconst } from '../../util/Vconst';
import { LPTopNavigation } from '../../pages/common/LPTopNavigation';
import { kitList } from '../../pages/Kits/kitList/kitList';

const protractor_1 = require("protractor");


let login = new loginpage();
let lpTopNavigation = new LPTopNavigation();
let lpKitList = new kitList();

// Kit List Page Regression
describe('Kit List Page Regression TST-1457', function () {

    it('STEP 1: Login as a LP user and navigate to kit list', async () => {

        console.log(" TST-1457 start");
        await browser.get(Vconst.url);
        browser.driver.manage().window().maximize();
        login.login(Vconst.D_username, Vconst.D_password);
        expect(browser.getCurrentUrl()).toContain('/Dashboard');

        lpTopNavigation.clickOnKitsTopMenuLink();
        lpTopNavigation.clickOnKitListMenuLink();
        browser.manage().timeouts().implicitlyWait(Vconst.lwaitTime);
    });

    it('STEP 2: Verify Actions button links and Columns', async () => {

        console.log(" TST-1457 STEP 2: verify action button links");
        lpKitList.verifyActionsButton();
        lpKitList.clickActionsButtonLink();
        lpKitList.verifyAddNewKitLink();
        lpKitList.verifyExportKitsLink();
        lpKitList.verifyKitConfigTempPasswordLink();
        lpKitList.verifyKitTypeMasterListLink();
        lpKitList.verifyColumnHamburgerMenuLink();
        lpKitList.verifyCustomerColumnLink();
        browser.manage().timeouts().implicitlyWait(Vconst.mwaitTime);
    });

    it('STEP 3: Click Go Button and Verify Grid', async () => {

        console.log(" TST-1457 STEP 3: go button and verify grid");
        lpKitList.clickOnGoButtonLink();
        lpKitList.verifyKitTableHeaderLink();
        browser.manage().timeouts().implicitlyWait(Vconst.mwaitTime);
    });

    it("Post-condition", async () => {


    });

});