import { browser, element, by } from 'protractor'
import { loginpage } from '../../pages/common/loginpage';
import { Vconst } from '../../util/Vconst';
import { LPTopNavigation } from '../../pages/common/LPTopNavigation';
import { deviceProductOrder } from '../../pages/Devices/PO/deviceProductOrderPage';

const protractor_1 = require("protractor");


let login = new loginpage();
let lpTopNavigation = new LPTopNavigation();
let devicePO = new deviceProductOrder();

// Devices PO Page Regression
describe('Receive Devices Page Regression TST-1416', function () {

    it('STEP 1: Login as a LP user and navigate to Receive Devices', async () => {

        console.log(" TST-1416 start");
        await browser.get(Vconst.url);
        browser.driver.manage().window().maximize();
        login.login(Vconst.D_username, Vconst.D_password);
        expect(browser.getCurrentUrl()).toContain('/Dashboard');

        lpTopNavigation.clickOnDevicesTopLink();
        lpTopNavigation.clickOnDevicesPO();
        browser.manage().timeouts().implicitlyWait(Vconst.lwaitTime);
    });

    it('STEP 2: Testing', async () => {

        console.log("TestingSearch")
        devicePO.clickOnSearchBar();
        console.log("TestingDropDown")
        devicePO.clickOnDropDown();
        console.log("TestingGo")
        devicePO.clickOnGoButtonLink();
        console.log("TestingHamburger")
        devicePO.clickHamburgerMenu();
        console.log("VerifyHamburger")
        devicePO.verifyHumburgerMenu();
        console.log("TestingActions")
        devicePO.clickActionsButtonLink();
        console.log("VerifyActions")
        devicePO.verifyActionsButton();
        console.log("Add PO")
        devicePO.clickAddPO();
        
        
    });
    /*
    it('STEP 2: Verify Actions button links and Columns', async () => {

        console.log(" TST-1416 STEP 2: verify action button links");
        lpKitList.verifyActionsButton();
        lpKitList.clickActionsButtonLink();
        lpKitList.verifyAddNewKitLink();
        lpKitList.verifyExportKitsLink();
        lpKitList.verifyKitConfigTempPasswordLink();
        lpKitList.verifyKitTypeMasterListLink();
        lpKitList.verifyColumnHamburgerMenuLink();
        lpKitList.verifyCustomerColumnLink();
        browser.manage().timeouts().implicitlyWait(Vconst.mwaitTime);
    });

    it('STEP 3: Click Go Button and Verify Grid', async () => {

        console.log(" TST-1443 STEP 3: go button and verify grid");
        lpKitList.clickOnGoButtonLink();
        lpKitList.verifyKitTableHeaderLink();
        browser.manage().timeouts().implicitlyWait(Vconst.mwaitTime);
    });

    it("Post-condition", async () => {


    });
*/
});