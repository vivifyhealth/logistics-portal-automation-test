import { ElementFinder, element, by, browser, ElementArrayFinder, ExpectedConditions } from "protractor";
import { loginpage } from '../../pages/common/loginpage';
import { Vconst } from '../../util/Vconst';
import { LPTopNavigation } from '../../pages/common/LPTopNavigation';
import { Customer } from '../../pages/Customers/customerPage';
import { forEachChild } from "typescript";
import exp from "constants";

const protractor_1 = require("protractor");


let login = new loginpage();
let lpTopNavigation = new LPTopNavigation();
let customer = new Customer();


// Customer Page Regression
describe('Customer Page Regression TST-1464', function () {

    it('STEP 1: Login as a LP user and navigate to Customer page', async () => {

        console.log(" TST-1464 start");
        await browser.get(Vconst.url);
        browser.driver.manage().window().maximize();
        login.login(Vconst.D_username, Vconst.D_password);
        expect(browser.getCurrentUrl()).toContain('/Dashboard');

        lpTopNavigation.clickOnCustomersLink();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    });

    it('STEP 2: Verify the hamburger menu', async () => {
        
        //Checks that the default columns are correct
        console.log("Testing Hamburger");
        customer.clickHamburgerMenu();
        customer.verifyHumburgerMenu();
        customer.verifyDefaultHeaders(); 
        //Have only the customerId column selected
        //customer.selectOnlyCustomerIdColumn();
        //customer.verifyOneHeader();  
        //Select all the columns and verify the headers are correct        
        //customer.selectAllColumns();
        //customer.verifyAllHeaders();
        });

            
    });

    /*it('STEP 3: Save customer details', async () => {

        //Identify the customer being used
        customer.columnHeaders
    })
*/