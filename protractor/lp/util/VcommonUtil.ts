import { Vconst } from "./Vconst";
import { ElementFinder, browser } from "protractor";
import { protractor } from "protractor/built/ptor";

/**
 * This Class is used to write the Common Util Methods which are used in the Automation
 */
export class VcommonUtil {

    public static computeName(name: string): string {

        name = name + Vconst.DateHelper.formatToISO(new Date());
        return name;
    }

    /**
     * This Method Populate the given date in the Date Text Box . 
     * 
     * @param element EliementFiner for the Date Text Box.
     * @param date Date object
     */
    public static sendKeysToDateTextBox(element: ElementFinder, date: Date) {

        element.clear();
        element.sendKeys(("0" + (date.getMonth() + 1)).slice(-2));
        element.sendKeys(protractor.Key.ESCAPE);
        browser.waitForAngular();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        element.sendKeys(("0" + date.getDate()).slice(-2));
        browser.waitForAngular();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        element.sendKeys(protractor.Key.ESCAPE);
        element.sendKeys(date.getFullYear());
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    /**
     * This Method will scroll the Browswer window to the 
     * specified x, y co ordinates. 
     */
    public static scrollTo(x: number, y: number) {
        var expression = 'window.scrollTo(' + x + ',' + y + ');'
        browser.executeScript(expression).then(function () {
            //console.log('Scrolling to (x,y) ('+x+','+y+')');
        });
    }


    static scrollDown() {
        browser.executeScript('window.scrollTo(0,10000);');
        Vconst.waitAngular(Vconst.swaitTime);
    }

    static scrollUp() {
        browser.executeScript('window.scrollTo(0,0);');
        Vconst.waitAngular(Vconst.swaitTime);
    }



}