import { browser, element, by } from 'protractor'
import { DriverProvider } from 'protractor/built/driverProviders';

export class Vconst {

    static swaitTime = 1000;
    static mwaitTime = 5000;
    static lwaitTime = 9000;
    /* static vivifyAdmin = 'a.mahajan';
    //static vivifyAdmin = 'vivifyadmin';
    static vivifyPassword = "Health11"
    // username, password  - for Vivify support user
    static username = 'auto_vsupport';    //'v.vuruma';  //'v.v1';      
    static password = 'Health11';      //'Health001';  //'Health001';   //'HealthVijaya1';          
    static DcontentAdm_username = 'auto_cadmin' */
    static D_username = 'lpautomation';
    static D_password = 'Health11';   //'Health11'; // Auto :'Vijaya12#$'; //Health15
    /* // clinical1 username and password
    static clinicalUserName = "auto_cl1"         //'vijaya1';
    static clinicalPassword = "Health11"        //'Health14';
    //clinical 2 username and password
    static clinicalUserName2 = "auto_cl2";
    static clinicalPassword2 = "Health11";
    // clinical 3 username and password
    static clinicalUserName23 = 'auto_cl3';  //vijaya2sitA
    static clinicalPassword23 = 'Health11'    //'Health321' 
    // ContentAdminuser
    static contentAdminUser = "auto_cadmin";
    static contentAdminPassword = "Health11";

    // ContentManager

    static contentManagerUser = "auto_cmanager";
    static contentMagerPassword = "Health11";

    static vijayaCMU = "autoV_cmanager";
    static vijayaCMP = "Health11";


    static loginUserLastName = "vuruma";
    static laginclinicalusername = "vijaya"
 */
    static url = `https://kitmaster.dev.vivifyhealth.com/Account/Login`;
    //static url = `${browser.baseUrl}/CaregiverPortal/index.html#/Login`;
    // static testotnUrl = `https://testotn.dev.vivifyhealth.com/CaregiverPortal/index.html#/Login`;
    

    // these 4 Varbilies are used for Sites Test cases
    static siteDefault = 'west autosite'; //''
    static siteA = 'East autoSite';      //'East Site';  //'';
    static siteB = 'South autoSite';
    static siteC = 'North autosite';
    // sitA specific  username and password  
    static v_sitAUsername = "auto_siteadmin";            //'sitAvijaya2';
    static v_sitAPassword = "Health11";                //'Health003';

    static currentDate1 = Math.round(new Date().getDate()).toString();
    static currentDate = ('0' + Vconst.currentDate1).slice(-2);
    static currentHours = Math.round(new Date().getHours()).toString();
    static currentMinutes = Math.round(new Date().getMinutes()).toString();
    static currentSeconds = Math.round(new Date().getSeconds()).toString();
    static currentTime = Vconst.currentDate + Vconst.currentHours + Vconst.currentMinutes + Vconst.currentSeconds;

    static P_lastName = "LName" + Vconst.currentHours + Vconst.currentMinutes

    static pastOneHour = Math.round(new Date().getHours() - 1).toString();

    static TimeHelper = {

        addHours: function (aDate: any, numberOfHours: any) {
            aDate.setDate(aDate.getHours() + numberOfHours);
            return aDate;
        }


    }



    static DateHelper = {

        addDays: function (aDate: Date, numberOfDays: any): Date {
            aDate.setDate(aDate.getDate() + numberOfDays); // Add numberOfDays
            return aDate;                                  // Return the date
        },
        format: function format(date: any) {
            return [
                ("0" + (date.getMonth() + 1)).slice(-2),   // Get month and pad it with zeroes
                ("0" + date.getDate()).slice(-2),           // Get day and pad it with zeroes                    
                date.getFullYear()                          // Get full year
            ].join('/');                                   // Glue the pieces together
        },
        futureDate: function futureDate(date: any) {
            return ("0" + date.getDate()).slice(-2)
        },

        formatToISO: function format(date: any) {
            return [
                ("0" + (date.getMonth() + 1)).slice(-2),   // Get month and pad it with zeroes
                ("0" + date.getDate()).slice(-2),           // Get day and pad it with zeroes                    
                date.getFullYear()                          // Get full year
            ].join('');
        },

        pastMmYear: function pastmmyear(date: Date, amountOfMonths: any) {
            return [
                ("0" + (date.getMonth() - amountOfMonths + 1)).slice(-2),
                date.getFullYear()
            ].join('/');
        },
        furureMmYear: function furureMmYear(date: Date, amountOfMonths: any) {
            return [
                ("0" + (date.getMonth() + amountOfMonths + 1)).slice(-2),
                date.getFullYear()
            ].join('/');
        },
        currentMmYear: function currentMmYear(date: Date) {
            return [
                ("0" + (date.getMonth() + 1)).slice(-2),
                date.getFullYear()
            ].join('/');
        },



        futureDays: function format(date: any, amountOfDays: any) {
            return [
                ("0" + (date.getMonth() + 1)).slice(-2),
                ("0" + (date.getDate() + amountOfDays)).slice(-2),
                date.getFullYear()
            ].join('/');
        },
        pastDays: function format(date: any, amountOfDays: any) {
            return [
                ("0" + (date.getMonth() + 1)).slice(-2),
                ("0" + (date.getDate() - amountOfDays)).slice(-2),
                date.getFullYear()
            ].join('/');
        },


    }



    static waitAngular(waittime: any) {
        browser.waitForAngular
        browser.sleep(waittime);
    }

    static refreshPage() {
        browser.refresh();
        browser.waitForAngular
        browser.sleep(this.swaitTime);

        //accept Server Error if displayed
        element(by.className('sweet-alert showSweetAlert visible')).isPresent().then(function (isPresent) {
            if (isPresent) {
                element(by.buttonText('OK')).click();
                browser.waitForAngular();
            }
        });
    }

    static DEVELOPER: string = "developer"
    static CLINICAL3: string = "clinical3"

}