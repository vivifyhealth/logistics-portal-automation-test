import { ElementFinder, element, by, browser, ElementArrayFinder, ExpectedConditions } from "protractor";
import { isParameter } from "typescript";
import { Vconst } from "../../../util/Vconst";
import { VcommonUtil } from "../../../util/VcommonUtil";

/**
 * 
 * 
 * Name             Jira #             Date                          Description 
 * ***********************************************************************************************
 * starnosky                           9/17/2021                 
 * 
 */

export class deviceProductOrder {
    searchBar: ElementFinder;
    dropDownMenu: ElementFinder;
    clearFilter: ElementFinder;
    completedFilter: ElementFinder;
    deletedFilter: ElementFinder;
    goButtonLink: ElementFinder;
    actionsButtonLink: ElementFinder;
    newPoButton: ElementFinder;
    exportList: ElementFinder;
    hamburgerMenu: ElementFinder;
    firstProductOrder: ElementFinder;
    firstPoNumber: ElementFinder;
    

    constructor() {
        this.searchBar = element(by.id("searchText"));
        this.dropDownMenu = element(by.className("btn btn-default dropdown-toggle"));
        this.clearFilter = element(by.xpath("//a[. = 'Clear filter']"));
        this.completedFilter = element(by.xpath("//a[. = 'Completed POs']"));
        this.deletedFilter = element(by.xpath("//a[. = 'Deleted POs']"));
        this.goButtonLink = element(by.buttonText("Go!"));
        this.actionsButtonLink = element(by.buttonText("Actions"));
        this.newPoButton = element(by.xpath("//a[. = 'New PO']"));
        this.exportList = element(by.xpath("//a[. = 'Export List']"));
        this.hamburgerMenu = element(by.className("btn btn-primary btn-xs dropdown-toggle"));
        this.firstProductOrder = element(by.xpath("//table[@class = 'table table-striped  table-responsive']/tbody/tr"));
        this.firstPoNumber = element(by.xpath("//table[@class = 'table table-striped  table-responsive']/tbody/tr/td"));


    }

    clickOnSearchBar() {
        this.searchBar.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnDropDown() {
        this.dropDownMenu.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
    
    clickOnGoButtonLink() {
        this.goButtonLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickActionsButtonLink()
    {
        this.actionsButtonLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyActionsButton()
    {
        expect(this.actionsButtonLink.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickAddPO()
    {
        this.newPoButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickExportList()
    {
        this.exportList.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickHamburgerMenu()
    {
        this.hamburgerMenu.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyHumburgerMenu()
    {
        expect(this.hamburgerMenu.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    openFirstPo()
    {
        this.firstProductOrder.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
   
}