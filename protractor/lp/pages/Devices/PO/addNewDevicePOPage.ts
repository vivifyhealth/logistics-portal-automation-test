import { ElementFinder, element, by, browser, ElementArrayFinder, ExpectedConditions } from "protractor";
import { isParameter } from "typescript";
import { Vconst } from "../../../util/Vconst";
import { VcommonUtil } from "../../../util/VcommonUtil";

/**
 * 
 * 
 * Name             Jira #             Date                          Description 
 * ***********************************************************************************************
 * starnosky                           9/20/2021                 
 * 
 */

export class addNewDevicePoPage {
    summaryTextbox: ElementFinder;
    vendorTextbox: ElementFinder;
    logisticsCenter: ElementFinder;
    referencePOTextbox: ElementFinder;
    condition: ElementFinder;
    owner: ElementFinder;
    descriptionTextbox: ElementFinder;
    addDeviceButton: ElementFinder;
    firstDeviceQuantityTextbox: ElementFinder;
    firstDeviceType: ElementFinder;
    firstDeviceModel: ElementFinder;
    addPartButton: ElementFinder;
    firstPartQuantityTextbox: ElementFinder;
    firstPartType: ElementFinder;
    firstPart: ElementFinder;
    saveButton: ElementFinder;
    cancelButton: ElementFinder;
    
    constructor() {
        this.summaryTextbox = element(by.id("Summary"));
        this.vendorTextbox = element(by.id("Vendor"));
        this.logisticsCenter = element(by.id("LogisticsLocationId"));
        this.referencePOTextbox = element(by.id("ReferenceIdent"));
        this.condition = element(by.id("InventoryConditionId"));
        this.owner = element(by.id("OwnedBy_Id"));
        this.descriptionTextbox = element(by.id("Description"));
        this.addDeviceButton = element(by.buttonText("Add Device"));
        this.firstDeviceQuantityTextbox = element(by.name('PurchaseOrderDevices[0].Count'));
        this.firstDeviceType = element(by.name("PurchaseOrderDevices[0].DeviceTypeEnumId"))
        this.firstDeviceModel = element(by.name("PurchaseOrderDevices[0].DeviceModelEnumId"));
        this.addPartButton = element(by.buttonText("Add Part"));
        this.firstPartQuantityTextbox = element(by.name('PurchaseOrderParts[0].Count'));
        this.firstPartType = element(by.name("PurchaseOrderParts[0].PartType"));
        this.firstPart = element(by.name("PurchaseOrderParts[0].DeviceNonSerialId"));
        this.saveButton = element(by.buttonText('Save'));
        this.cancelButton = element(by.xpath("//*[@id='InventoryPOForm']/div[11]/div/div/a"));
    }

    setSummary(Summary: string)
    {
        this.summaryTextbox.sendKeys(Summary);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setVendor(Vendor: string)
    {
        this.vendorTextbox.sendKeys(Vendor);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
    
    clickLogisticsLocation(LogisticsCenter: string)
    {
        this.logisticsCenter.element(by.cssContainingText('option', LogisticsCenter)).click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setReferencePO(referencePO: string)
    {
        this.referencePOTextbox.sendKeys(referencePO);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickCondition(condition: string)
    {
        this.condition.element(by.cssContainingText('option', condition)).click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOwner(owner: string)
    {
        this.owner.element(by.cssContainingText('option', owner)).click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setDescription(description: string)
    {
        this.descriptionTextbox.sendKeys(description);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickAddDevice()
    {
        this.addDeviceButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setFirstDeviceQuantity(DeviceInputQuantity: string)
    {
        this.firstDeviceQuantityTextbox.sendKeys(DeviceInputQuantity);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setFirstDeviceType(DeviceType: string)
    {
        this.firstDeviceType.element(by.cssContainingText('option', DeviceType)).click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setFirstDeviceModel(DeviceModel: string)
    {
        this.firstDeviceModel.element(by.cssContainingText('option', DeviceModel)).click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickAddPart()
    {
        this.addPartButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setFirstPartQuantity(PartInputQuantity: string)
    {
        this.firstPartQuantityTextbox.sendKeys(PartInputQuantity);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setFirstPartType(PartType: string)
    {
        this.firstPartType.element(by.cssContainingText('option', PartType)).click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setFirstPartModel(PartModel: string)
    {
        this.firstPart.element(by.cssContainingText('option', PartModel)).click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickCancel()
    {
        this.cancelButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickSave()
    {
        this.saveButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

}