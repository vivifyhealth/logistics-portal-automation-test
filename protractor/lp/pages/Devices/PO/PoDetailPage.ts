import { ElementFinder, element, by, browser, ElementArrayFinder, ExpectedConditions } from "protractor";
import { ElementFlags, isParameter, TypeVariable, VariableDeclaration } from "typescript";
import { Vconst } from "../../../util/Vconst";
import { VcommonUtil } from "../../../util/VcommonUtil";


/**
 * 
 * 
 * Name             Jira #             Date                          Description 
 * ***********************************************************************************************
 * starnosky                           9/21/2021                 
 * 
 */

export class PoDetailPage {
    successBanner: ElementFinder;
    closeSuccessButton: ElementFinder;
    actionButton: ElementFinder;
    actionDropDown: ElementFinder;
    editPO: ElementFinder;
    completePO: ElementFinder;
    deletePO: ElementFinder;
    pageHeader: ElementFinder;
    PoSummary: ElementFinder;
    details1: ElementFinder;
    details2: ElementFinder;
    PoDescription: ElementFinder;
    firstDevicesOrdered: ElementFinder;
    firstDevicesReceived: ElementFinder;
    firstDevicesType: ElementFinder;
    firstDevicesModel: ElementFinder;
    firstDevicesPartNum: ElementFinder;
    firstReceiveDevicesButton: ElementFinder;
    secondDevicesOrdered: ElementFinder;
    secondDevicesReceived: ElementFinder;
    secondDevicesType: ElementFinder;
    secondDevicesModel: ElementFinder;
    //secondDevicesPartNum: ElementFinder;
    secondReceiveDevicesButton: ElementFinder;
    addNoteButton: ElementFinder;
    noteTextArea: ElementFinder;
    saveNote: ElementFinder;
    savedNote: ElementFinder;
    historyText: ElementFinder;
    
    constructor() {
        this.successBanner = element(by.className("alert alert-success alert-dismissible"));
        this.closeSuccessButton = element(by.className("close"));
        //Actions
        this.actionButton = element(by.buttonText("Actions"));
        this.actionDropDown = element(by.className("dropdown-menu pull-right"));
        this.editPO = element(by.xpath("//a[. = 'Edit']"));
        this.completePO = element(by.xpath("//a[. = 'Mark Complete']"));
        this.deletePO = element(by.xpath("//a[. = 'Delete Order']"));
        //Details
        this.pageHeader = element(by.className("panel-header"));
        this.PoSummary = element(by.className("col-sm-12"));
        this.details1 = element(by.className("col-sm-4"));
        this.details2 = element(by.className("col-sm-6"));
        this.PoDescription = element.all(by.className("col-sm-12")).get(1);
        //Device area -- Getting elements needs improvement, subject to change based on several factors
        this.firstDevicesOrdered = element.all(by.className("col-sm-1")).get(3);
        this.firstDevicesReceived = element.all(by.className("col-sm-1")).get(4);
        this.firstDevicesType = element.all(by.className("col-sm-3")).get(2);
        this.firstDevicesModel = element.all(by.className("col-sm-3")).get(3);
        this.firstDevicesPartNum = element.all(by.className("col-sm-1")).get(5);
        this.firstReceiveDevicesButton = element(by.className("btn btn-primary btn-xs"));
        this.secondDevicesOrdered = element.all(by.className("col-sm-1")).get(6);
        this.secondDevicesReceived = element.all(by.className("col-sm-1")).get(7);
        this.secondDevicesType = element.all(by.className("col-sm-3")).get(4);
        this.secondDevicesModel = element.all(by.className("col-sm-4")).get(1);
        //this.secondDevicesPartNum = element.all(by.className("col-sm-1")).get(12);
        this.secondReceiveDevicesButton = element(by.className("btn btn-primary btn-xs"));
        //Note Area
        this.addNoteButton = element(by.className("glyphicon glyphicon-plus"));
        this.noteTextArea = element(by.id("newNote"));
        this.saveNote = element(by.buttonText("Save Note"));
        this.savedNote = element(by.className("col-md-8"));
        //History
        this.historyText = element(by.className("col-md-8"));
        
    }

    verifySuccess() 
    {
        expect(this.successBanner.isEnabled()).toBe(true);
        console.log("Banner is Present");
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    closeSucessBanner() 
    {
        this.closeSuccessButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);

        expect(this.successBanner.isPresent()).toBe(false);
        console.log("Banner has closed");
    }

    openActionsMenu()
    {
        this.actionButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);

        expect(this.actionDropDown.isDisplayed()).toBe(true);
    }

    closeActionsMenu()
    {
        this.actionButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);

        expect(this.actionDropDown.isDisplayed()).toBe(false);
    }



    clickReceiveDevices()
    {
        this.firstReceiveDevicesButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);

        expect(browser.getCurrentUrl()).toContain('New?');
    }

    clickAddNote()
    {
        this.addNoteButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);

        expect(this.noteTextArea.isDisplayed()).toBe(true);
    }

    enterNote(note: string)
    {
        this.noteTextArea.sendKeys(note);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickSaveNote()
    {
        this.saveNote.click();
        browser.manage().timeouts().implicitlyWait(Vconst.mwaitTime);

        expect(this.noteTextArea.isDisplayed()).toBe(false);
    }
    
}


