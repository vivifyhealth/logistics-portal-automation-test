import { ElementFinder, element, by, browser, ElementArrayFinder, ExpectedConditions } from "protractor";
import { isParameter } from "typescript";
import { Vconst } from "../../util/Vconst";
import { VcommonUtil } from "../../util/VcommonUtil";

/**
 * This class contains LP top navigation links.  
 * 
 * Name             Jira #             Date                          Description 
 * ***********************************************************************************************
 * fstadler                            7/26/2021                 
 * starnosky                           9/17/2021                     Added 'Devices PO' and 'Receive Devices' page references
 * starnosky                           10/4/2021                     Added the rest of the top links
 */

export class LPTopNavigation {
    logisticsLink: ElementFinder;
    logisticsDashboardLink: ElementFinder;
    logisticsKitPOLink: ElementFinder;
    logisticsCreateKitsLink: ElementFinder;
    logisticsReceivingLink: ElementFinder;
    logisticsReprocessingLink: ElementFinder;
    logisticsShippingLink: ElementFinder;
    fulfillmentsLink: ElementFinder;
    devicesTopLink: ElementFinder;
    devicesDashboardLink: ElementFinder;
    devicesPOLink: ElementFinder;
    devicesDevicesLink: ElementFinder;
    devicesPartsLink: ElementFinder;
    receiveDevicesLink: ElementFinder;
    kitsTopLink: ElementFinder;
    kitsDashboardLink: ElementFinder;
    kitListLink: ElementFinder;
    kitStorageLink: ElementFinder;
    kitTypeMasterLink: ElementFinder;
    customersLink: ElementFinder;
    reportsLink: ElementFinder;
    billingLink: ElementFinder;
    usersLink: ElementFinder;
    rulesLink: ElementFinder;

    navigationItem = {
        NavItem: function NavItem(navItemName: string): ElementFinder {
            return element(by.linkText(navItemName))
        }

    }

    navigationSubItem = {

        NavSubItem: function NavSubItem(navSubItemName: string): any {

            return element(by.linkText(navSubItemName));


        }
    }

    navigationSubSubItem = {

        NavSubSubItem: function NavSubSubItem(navSubItemName: string): any {

            return element(by.cssContainingText('a[ng-click="vm.populationClicked(templategroup)"]', navSubItemName));

        }
    }
    constructor() {
        this.logisticsLink = element(by.css("#Menu-KitLogistics > a"));
        this.logisticsDashboardLink = element(by.css("#Menu-KitLogistics > ul > li:nth-child(1) > a"));
        this.logisticsKitPOLink = element(by.css("#Menu-KitLogistics > ul > li:nth-child(2) > a"));
        this.logisticsCreateKitsLink = element(by.css("#Menu-KitLogistics > ul > li:nth-child(3) > a"));
        this.logisticsReceivingLink = element(by.css("#Menu-KitLogistics > ul > li:nth-child(4) > a"));
        this.logisticsReprocessingLink = element(by.css("#Menu-KitLogistics > ul > li:nth-child(5) > a"));
        this.logisticsShippingLink = element(by.css("#Menu-KitLogistics > ul > li:nth-child(6) > a"));
        this.fulfillmentsLink = element(by.css("#Menu-KitLogistics > ul > li:nth-child(7) > a"));
        this.devicesTopLink = element(by.css("#Menu-Inventory > a"));
        this.devicesDashboardLink = element(by.css("#Menu-Inventory > ul > li:nth-child(1) > a"));
        this.devicesPOLink = element(by.css("#Menu-Inventory > ul > li:nth-child(2) > a"));
        this.devicesDevicesLink = element(by.css("#Menu-Inventory > ul > li:nth-child(3) > a"));
        this.devicesPartsLink = element(by.css("#Menu-Inventory > ul > li:nth-child(4) > a"));
        this.receiveDevicesLink = element(by.css("#Menu-Inventory > ul > li:nth-child(5) > a"));
        this.kitsTopLink = element(by.linkText('Kits'));
        this.kitsDashboardLink = element(by.css("#Menu-Kits > ul > li:nth-child(1) > a"));
        this.kitListLink = element(by.linkText('Kit List'));
        this.kitStorageLink = element(by.css("#Menu-Kits > ul > li:nth-child(3) > a"));
        this.kitTypeMasterLink = element(by.linkText('Kit Type Master List'));
        this.customersLink = element(by.id('Menu-Customers'));
        this.reportsLink = element(by.id('Menu-Reports'));
        this.billingLink = element(by.id('Menu-Billing'));
        this.usersLink = element(by.id('Menu-Users'));
        this.rulesLink = element(by.id('Menu-Rules'));
        
    }

    clickOnLogisticsMenuLink() {
        this.logisticsLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnLogisticsDashboard() {
        this.logisticsDashboardLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnLogisticsKitPO() {
        this.logisticsKitPOLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnLogisticsCreateKit() {
        this.logisticsCreateKitsLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnLogisticsReceiving() {
        this.logisticsReceivingLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnLogisticsReprocesing() {
        this.logisticsReprocessingLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnLogisticsShipping() {
        this.logisticsShippingLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnFulfillmentOrdersMenuLink() {
        this.fulfillmentsLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnDevicesTopLink() {
        this.devicesTopLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnDevicesPO() {
        this.devicesPOLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnDevicesPage() {
        this.devicesDevicesLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnDevicesPartsPage() {
        this.devicesPartsLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnReceiveDevices() {
        this.receiveDevicesLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnKitsTopMenuLink() {
        this.kitsTopLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnKitsDashboard() {
        this.kitsDashboardLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnKitListMenuLink() {
        this.kitListLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnKitStorage() {
        this.kitStorageLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnCustomersLink() {
        this.customersLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnReportsLink() {
        this.reportsLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnBillingLInk() {
        this.billingLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnUsersLink() {
        this.usersLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnRulesLink() {
        this.rulesLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

}