import { ElementFinder, element, by, browser } from "protractor"
import { Vconst } from "../../util/Vconst";
import { brotliCompress } from "zlib";

/**
 * This class contains the login Elements and login functionality for the given Username , password. 
 * 
 * Name             Jira #             Date                          Description 
 * ***********************************************************************************************
 * fstadler                               7/26/2021                 Initial class developed.
 * 
 */
export class loginpage {
    username: ElementFinder;
    password: ElementFinder;
    loginbutton: ElementFinder;
    serverError: ElementFinder;


    constructor() {

        this.username = element(by.id("UserName"));
        this.password = element(by.id("Password"));
        this.loginbutton = element(by.className("btn-lg btn-primary"));
        this.serverError = element(by.className('alert-danger'));
    }

    /**
     * 
     * This method is used to process/ exeute the login page
     * @param username 
     * @param password 
     */
    login(username: string, password: string) {
        console.log("Inside Login")
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime)
        browser.getCurrentUrl().then(function (txt) {
            console.log("In URL " + txt);
        })

        browser.getCurrentUrl()
            this.username.sendKeys(username);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        this.password.sendKeys(password);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        this.loginbutton.click().then(function () {
            browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
        }).catch(function (ex) {
            console.log("Exception in Logging in" + ex);
            // throw ex;
        });
    

    }


}