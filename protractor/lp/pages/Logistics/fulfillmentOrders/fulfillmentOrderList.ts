import { ElementFinder, element, by, browser, ElementArrayFinder, ExpectedConditions } from "protractor";
import { isParameter } from "typescript";
import { Vconst } from "../../../util/Vconst";
import { VcommonUtil } from "../../../util/VcommonUtil";

/**
 * 
 * 
 * Name             Jira #             Date                          Description 
 * ***********************************************************************************************
 * fstadler                               7/27/2021                 
 * 
 */

export class fulfillmentOrderList {
    actionsMenuLink: ElementFinder;
    createDeviceOrderLink: ElementFinder;


  
    constructor() {
        this.actionsMenuLink = element(by.id("__BVID__22__BV_toggle_"));
        this.createDeviceOrderLink = element(by.id("CreateNewOrder"));

    }

    clickOnActionsMenuLink() {
        this.actionsMenuLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnCreateDeviceOrderMenuLink() {
        this.createDeviceOrderLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    
}