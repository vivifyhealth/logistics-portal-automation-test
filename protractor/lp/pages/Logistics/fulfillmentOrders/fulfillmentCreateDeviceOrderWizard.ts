import { ElementFinder, element, by, browser, ElementArrayFinder, ExpectedConditions } from "protractor";
import { isParameter } from "typescript";
import { Vconst } from "../../../util/Vconst";
import { VcommonUtil } from "../../../util/VcommonUtil";
import { ALL } from "dns";

/**
 * 
 * 
 * Name             Jira #             Date                          Description 
 * ***********************************************************************************************
 * fstadler                               7/27/2021                 
 * 
 */

export class fulfillmentCreateDeviceOrderWizard {
    customerNextButton: ElementFinder;
    addDevicesNextButton: ElementFinder;
    addDeviceButton: ElementFinder;
    firstDeviceQuantityTextbox: ElementFinder;
    firstDeviceTypeSelect: ElementFinder;
    firstDeviceModel: ElementFinder;
    firstNameTextbox: ElementFinder;
    lastNameTextbox: ElementFinder;
    addressLine1Textbox: ElementFinder;
    addressLine2Textbox: ElementFinder;
    cityTextbox: ElementFinder;
    stateTextbox: ElementFinder;
    zipCodeTextbox: ElementFinder;
    phone1Textbox: ElementFinder;
    phone2Textbox: ElementFinder;
    shippingInformationNextButton: ElementFinder;
    confirmButton: ElementFinder;

    constructor() {
        this.customerNextButton = element(by.buttonText('Next'));
        this.addDevicesNextButton = element(by.buttonText('Next'));
        this.addDeviceButton = element(by.buttonText('+ Add Device'));
        this.firstDeviceQuantityTextbox = element(by.css('input[data-v-7ef87a0a]'));
        this.firstDeviceTypeSelect = element(by.css('select[data-v-7ef87a0a]'));
        //get second select element of that row
        this.firstDeviceModel = element.all(by.css('select[data-v-7ef87a0a]')).get(1);
        this.firstNameTextbox = element(by.css('input[placeholder="First Name"]'));
        this.lastNameTextbox = element(by.css('input[placeholder="Last Name"]'));
        this.addressLine1Textbox = element(by.css('input[placeholder="Address Line 1"]'));
        this.addressLine2Textbox = element(by.css('input[placeholder="Address Line 2"]'));
        this.cityTextbox = element(by.css('input[placeholder="City"]'));
        this.stateTextbox = element(by.css('input[placeholder="State"]'));
        this.zipCodeTextbox = element(by.css('input[placeholder="Zip Code"]'));
        this.phone1Textbox = element(by.css('input[placeholder="Phone Number with dashes"]'));
        this.phone2Textbox = element(by.css('input[placeholder="Phone Number 2 with dashes"]'));
        this.shippingInformationNextButton = element(by.buttonText('Next'));
        this.confirmButton = element(by.buttonText('Confirm'));
    }

    clickOnCustomerListForCustomer(customerName: string) 
    {
        element(by.cssContainingText('option', customerName)).click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

     clickOnManufacturer(manufacturerName: string) 
    {
        element(by.cssContainingText('option', manufacturerName)).click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickCustomerNextButton()
    {
        this.customerNextButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyCustomerNextButton()
    {
        expect(this.customerNextButton.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyAddDevicesNextButton()
    {
        expect(this.addDevicesNextButton.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickAddDevicesNextButton()
    {
        this.addDevicesNextButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyShippingNextButton()
    {
        expect(this.shippingInformationNextButton.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickShippingNextButton()
    {
        this.shippingInformationNextButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyConfirmButton()
    {
        expect(this.confirmButton.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickConfirmButton()
    {
        this.confirmButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickAddDeviceButton() 
    {
        this.addDeviceButton.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setFirstDeviceQuantity(inputQuantity: string)
    {
        this.firstDeviceQuantityTextbox.sendKeys(inputQuantity);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setFirstName(firstName: string)
    {
        this.firstNameTextbox.sendKeys(firstName);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setLastName(lastName: string)
    {
        this.lastNameTextbox.sendKeys(lastName);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setAddressLine1(addressLine1: string)
    {
        this.addressLine1Textbox.sendKeys(addressLine1);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setAddressLine2(addressLine2: string)
    {
        this.addressLine2Textbox.sendKeys(addressLine2);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setCity(city: string)
    {
        this.cityTextbox.sendKeys(city);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setState(state: string)
    {
        this.stateTextbox.sendKeys(state);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setZipCode(zipCode: string)
    {
        this.zipCodeTextbox.sendKeys(zipCode);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setPhone1(phone1: string)
    {
        this.phone1Textbox.sendKeys(phone1);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setPhone2(phone2: string)
    {
        this.phone2Textbox.sendKeys(phone2);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    setFirstDeviceModel(inputDeviceModel: string)
    {
        this.firstDeviceModel.element(by.cssContainingText('option', inputDeviceModel)).click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickOnFirstDeviceType(deviceType: string) {
        this.firstDeviceTypeSelect.element(by.cssContainingText('option', deviceType)).click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
}