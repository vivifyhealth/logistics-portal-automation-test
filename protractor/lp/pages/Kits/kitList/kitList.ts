import { ElementFinder, element, by, browser, ElementArrayFinder, ExpectedConditions } from "protractor";
import { isParameter } from "typescript";
import { Vconst } from "../../../util/Vconst";
import { VcommonUtil } from "../../../util/VcommonUtil";

/**
 * 
 * 
 * Name             Jira #             Date                          Description 
 * ***********************************************************************************************
 * fstadler                               8/9/2021                 
 * 
 */

export class kitList {
    goButtonLink: ElementFinder;
    actionsButtonLink: ElementFinder;
    addNewKitLink: ElementFinder;
    exportKitsLink: ElementFinder;
    kitConfigTempPasswordLink: ElementFinder;
    kitTypeMasterListLink: ElementFinder;
    columnHamburgerMenuLink: ElementFinder;
    customerColumnLink: ElementFinder;
    kitTableHeaderLink: ElementFinder;

    constructor() {
        this.goButtonLink = element(by.buttonText("Go!"));
        this.actionsButtonLink = element(by.buttonText("Actions"));
        this.addNewKitLink = element(by.linkText("Add new kit"));
        this.exportKitsLink = element(by.linkText("Export Kits"));
        this.kitConfigTempPasswordLink = element(by.linkText("Kit Config Temp Password"));
        this.kitTypeMasterListLink = element(by.linkText("Kit Type Master List"));
        this.columnHamburgerMenuLink = element(by.id("__BVID__23__BV_toggle_"));
        this.customerColumnLink = element(by.xpath("//label[. = 'Customer']"));
        this.kitTableHeaderLink = element(by.xpath("//th[. = 'Kit']"));
    }

    clickOnGoButtonLink() {
        this.goButtonLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    clickActionsButtonLink()
    {
        this.actionsButtonLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyActionsButton()
    {
        expect(this.actionsButtonLink.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyAddNewKitLink()
    {
        expect(this.addNewKitLink.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyExportKitsLink()
    {
        expect(this.exportKitsLink.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyKitConfigTempPasswordLink()
    {
        expect(this.kitConfigTempPasswordLink.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyKitTypeMasterListLink()
    {
        expect(this.kitTypeMasterListLink.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyColumnHamburgerMenuLink()
    {
        expect(this.columnHamburgerMenuLink.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyCustomerColumnLink()
    {
        expect(this.customerColumnLink.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyKitTableHeaderLink()
    {
        expect(this.kitTableHeaderLink.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
}