import { ElementFinder, element, by, browser, ExpectedConditions, ElementArrayFinder } from "protractor";
import { isParameter } from "typescript";
import { Vconst } from "../../util/Vconst";
import { VcommonUtil } from "../../util/VcommonUtil";
import { ALL } from "dns";
import { Then } from "cucumber";

/**
 * 
 * 
 * Name             Jira #             Date                          Description 
 * ***********************************************************************************************
 * starnosky                           10/20/2021                 
 * 
 */

export class Customer {
    searchBar: ElementFinder;
    dropDownFilterMenu: ElementFinder;
    clearFilter: ElementFinder;
    prodFilter: ElementFinder;
    goButtonLink: ElementFinder;
    actionsButtonLink: ElementFinder;
    newCustomer: ElementFinder;
    exportList: ElementFinder;
    goPatientSearch: ElementFinder;
    hamburgerMenu: ElementFinder;
    checkedColumns: ElementArrayFinder;
    typeColumn: ElementFinder;
    dataCenterColumn: ElementFinder;
    versionColumn: ElementFinder;
    urlColumn: ElementFinder;
    logisticsColumn: ElementFinder;
    reprocessingColumn: ElementFinder;
    videoConfColumn: ElementFinder;
    sellerColumn: ElementFinder;
    createdDateColumn: ElementFinder;
    createdByColumn: ElementFinder;
    modifiedColumn: ElementFinder;
    modifiedByColumn: ElementFinder;
    locationsColumn: ElementFinder;
    kitsColumn: ElementFinder;
    logisticsCenterColumn: ElementFinder;
    customerIdColumn: ElementFinder;
    columnHeaders: ElementFinder;
    selectedCustomer: ElementArrayFinder;

    constructor() {
        this.searchBar = element(by.id("searchText"));
        this.dropDownFilterMenu = element(by.className("input-group-btn open"));
        this.clearFilter = element(by.xpath("//a[. = 'Clear filter']"));
        this.prodFilter = element(by.xpath("//a[. = 'Production customers']"));
        this.goButtonLink = element(by.buttonText("Go!"));
        this.actionsButtonLink = element(by.buttonText("Actions"));
        this.newCustomer = element(by.xpath("//a[. = 'Create new customer']"));
        this.exportList = element(by.buttonText("//a[. = 'Export list']"));
        this.goPatientSearch = element(by.xpath("//a[. = 'Go Patient search']"));
        this.hamburgerMenu = element(by.className("btn btn-primary btn-xs dropdown-toggle"));
        this.checkedColumns = element.all(by.xpath("//input[@type = 'checkbox']"));
        this.typeColumn = element(by.xpath("//input[@value = 'TYPE']"));
        this.dataCenterColumn = element(by.xpath("//input[@value = 'DATACENTER']"));
        this.versionColumn = element(by.xpath("//input[@value = 'VERSION']"));
        this.urlColumn = element(by.xpath("//input[@value = 'URL']"));
        this.logisticsColumn = element(by.xpath("//input[@value = 'LOGISTICS']"));
        this.reprocessingColumn = element(by.xpath("//input[@value = 'REPROCESSING']"));
        this.videoConfColumn = element(by.xpath("//input[@value = 'VIDEOCONF']"));
        this.sellerColumn = element(by.xpath("//input[@value = 'SELLER']"));
        this.createdDateColumn = element(by.xpath("//input[@value = 'CREATED']"));
        this.createdByColumn = element(by.xpath("//input[@value = 'CREATEDBY']"));
        this.modifiedColumn = element(by.xpath("//input[@value = 'MODIFIED']"));
        this.modifiedByColumn = element(by.xpath("//input[@value = 'MODIFIEDBY']"));
        this.locationsColumn = element(by.xpath("//input[@value = 'LOCATIONS']"));
        this.kitsColumn = element(by.xpath("//input[@value = 'KITS']"));
        this.logisticsCenterColumn = element(by.xpath("//input[@value = 'LOGISTICSCENTER']"));
        this.customerIdColumn = element(by.xpath("//input[@value = 'ID']"));
        this.columnHeaders = element(by.xpath("//table[@class = 'table table-striped']/thead/tr"));
        this.selectedCustomer = element.all(by.xpath("//table[@class = 'table table-striped']/tbody/tr/td"));
    }

    clickOnSearchBar() {
        this.searchBar.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
    clickOnDropDown() {
        this.dropDownFilterMenu.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
    clickOnGoButtonLink() {
        this.goButtonLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
    clickActionsButtonLink() {
        this.actionsButtonLink.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
    verifyActionsButton() {
        expect(this.actionsButtonLink.isEnabled()).toBe(true);
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
    clickAddCustomer() {
        this.newCustomer.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
    clickExportList() {
        this.exportList.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
    clickGoPatientSearch() {
        this.goPatientSearch.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
    verifyDefaultHeaders(){

        this.checkedColumns.count().then(function(length){
            for (let i = 0; i < length; i++){
                let checkBox = element.all(by.xpath("//input[@type = 'checkbox']")).get(i);
                checkBox.getAttribute("checked").then(function(isChecked:string){
                    if (isChecked){    
                        checkBox.getAttribute('value').then(function(checkedColumn){                            
                            checkedColumn = checkedColumn.charAt(0).toUpperCase()+ checkedColumn.substring(1).toLowerCase();
                            if (checkedColumn.match('Url')){
                                expect(element.all(by.xpath("//table[@class = 'table table-striped']/thead/tr/th")).getText()).toContain('Portal Url');
                                checkedColumn.replace(checkedColumn,'Portal Url');
                                console.log('Url Column is present');
                            }
                            else{
                                let header = element.all(by.xpath("//table[@class = 'table table-striped']/thead/tr/th"));
                                header.getText().then(function(name){
                                    console.log(name);
                                    console.log(checkedColumn);
                                    expect(name).toContain(checkedColumn);
                                });   
                            }                                                                                
                        });
                    }
                });
            }
        });
        
        //expect(this.columnHeaders.getText()).toContain('Name Seller Type Version Portal Url Logistics Reprocessing Customer Id');
    }
    verifyAllHeaders(){
        expect(this.columnHeaders.getText()).toContain('Name Type Data Center Version Portal Url Logistics Reprocessing Video Conf Seller Created Date Created By Modified Modified By Locations Customer Id');
    }
    verifyOneHeader(){
        expect(this.columnHeaders.getText()).toContain('Name CustomerId'); 
    }
    clickHamburgerMenu() {
        this.hamburgerMenu.click();
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }

    verifyHumburgerMenu() {
        expect(this.hamburgerMenu.isEnabled()).toBe(true);
        this.typeColumn.isPresent();
        this.dataCenterColumn.isPresent;
        this.versionColumn.isPresent;
        this.urlColumn.isPresent;
        this.logisticsColumn.isPresent;
        this.reprocessingColumn.isPresent;
        this.videoConfColumn.isPresent;
        this.sellerColumn.isPresent;
        this.createdDateColumn.isPresent;
        this.createdByColumn.isPresent;
        this.modifiedColumn.isPresent;
        this.modifiedByColumn.isPresent;
        this.locationsColumn.isPresent;
        this.kitsColumn.isPresent;
        this.logisticsCenterColumn.isPresent;
        this.customerIdColumn.isPresent;
        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
    }
    selectAllColumns(){
        //Check to see if the column is selected
        //Select the column if it isn't
        this.checkedColumns.count().then(function(length){
            for (let i = 0; i < length; i++){
                let checkBox = element.all(by.xpath("//input[@type = 'checkbox']")).get(i);
                checkBox.getAttribute("checked").then(function(isChecked:string){
                    if (isChecked){    
                        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
                    }
                    else{
                        checkBox.click();
                        browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
                    } 
                });
            }
        });
    }
    selectOnlyCustomerIdColumn(){async () => 
        //Check to see if the column is selected
        //Select the column if it isn't
        {
            if (await this.typeColumn.getattribute('checked'))
            {
                this.typeColumn.click()
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.sleep(Vconst.swaitTime)
            }
            if (await this.dataCenterColumn.getAttribute('checked'))
            {
                this.dataCenterColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.versionColumn.getAttribute('checked'))
            {
                this.versionColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.urlColumn.getAttribute('checked'))
            {
                this.urlColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.logisticsColumn.getAttribute('checked'))
            {
                this.logisticsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.reprocessingColumn.getAttribute('checked'))
            {
                this.reprocessingColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.videoConfColumn.getAttribute('checked'))
            {
                this.videoConfColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.sellerColumn.getAttribute('checked'))
            {
                this.sellerColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.createdDateColumn.getAttribute('checked'))
            {
                this.createdDateColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.createdByColumn.getAttribute('checked'))
            {
                this.createdByColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.modifiedColumn.getAttribute('checked'))
            {
                this.modifiedColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.modifiedByColumn.getAttribute('checked'))
            {
                this.modifiedByColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.locationsColumn.getAttribute('checked'))
            {
                this.locationsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.kitsColumn.getAttribute('checked'))
            {
                this.kitsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.logisticsCenterColumn.getAttribute('checked'))
            {
                this.logisticsCenterColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.customerIdColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.customerIdColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
        }
    }
    selectNoColumns(){async () => 
        //Check to see if the column is selected
        //Select the column if it is
        {
            if (await this.typeColumn.getattribute('checked'))
            {
                this.typeColumn.click()
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.sleep(Vconst.swaitTime)
            }
            if (await this.dataCenterColumn.getAttribute('checked'))
            {
                this.dataCenterColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.versionColumn.getAttribute('checked'))
            {
                this.versionColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.urlColumn.getAttribute('checked'))
            {
                this.urlColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.logisticsColumn.getAttribute('checked'))
            {
                this.logisticsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.reprocessingColumn.getAttribute('checked'))
            {
                this.reprocessingColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.videoConfColumn.getAttribute('checked'))
            {
                this.videoConfColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.sellerColumn.getAttribute('checked'))
            {
                this.sellerColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.createdDateColumn.getAttribute('checked'))
            {
                this.createdDateColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.createdByColumn.getAttribute('checked'))
            {
                this.createdByColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.modifiedColumn.getAttribute('checked'))
            {
                this.modifiedColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.modifiedByColumn.getAttribute('checked'))
            {
                this.modifiedByColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.locationsColumn.getAttribute('checked'))
            {
                this.locationsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.kitsColumn.getAttribute('checked'))
            {
                this.kitsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.logisticsCenterColumn.getAttribute('checked'))
            {
                this.logisticsCenterColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.customerIdColumn.getAttribute('checked'))
            {
                this.customerIdColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
        }
    }
    selectOriginalDefault(){async () => 
        //Have 'Type' 'Version' 'Portal Url' 'Logistics' 'Reprocessing' 'Seller' 'Customer ID' columns selected
        //All else are not selected
        {
            if (await this.typeColumn.getattribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.typeColumn.click()
                browser.sleep(Vconst.swaitTime)
            }
            if (await this.dataCenterColumn.getAttribute('checked'))
            {
                
                this.dataCenterColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.versionColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.versionColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.urlColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.urlColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.logisticsColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.logisticsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.reprocessingColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.reprocessingColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.videoConfColumn.getAttribute('checked'))
            {
                this.videoConfColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.sellerColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.sellerColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.createdDateColumn.getAttribute('checked'))
            {
                this.createdDateColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.createdByColumn.getAttribute('checked'))
            {
                this.createdByColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.modifiedColumn.getAttribute('checked'))
            {
                this.modifiedColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.modifiedByColumn.getAttribute('checked'))
            {
                this.modifiedByColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.locationsColumn.getAttribute('checked'))
            {
                this.locationsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.kitsColumn.getAttribute('checked'))
            {
                this.kitsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.logisticsCenterColumn.getAttribute('checked'))
            {
                this.logisticsCenterColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.customerIdColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.customerIdColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
        }
    } 
    selectNewDefault(){async () => 
        //Have 'Type' 'Version' 'Portal Url' 'Logistics' 'Reprocessing' 'Seller' 'Customer ID' columns selected
        //All else are not selected
        {
            if (await this.typeColumn.getattribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.typeColumn.click()
                browser.sleep(Vconst.swaitTime)
            }
            if (await this.dataCenterColumn.getAttribute('checked'))
            {
                
                this.dataCenterColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.versionColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.versionColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.urlColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.urlColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.logisticsColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.logisticsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.reprocessingColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.reprocessingColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.videoConfColumn.getAttribute('checked'))
            {
                this.videoConfColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.sellerColumn.getAttribute('checked'))
            {
                this.sellerColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.createdDateColumn.getAttribute('checked'))
            {
                this.createdDateColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.createdByColumn.getAttribute('checked'))
            {
                this.createdByColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.modifiedColumn.getAttribute('checked'))
            {
                this.modifiedColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.modifiedByColumn.getAttribute('checked'))
            {
                this.modifiedByColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.locationsColumn.getAttribute('checked'))
            {
                this.locationsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.kitsColumn.getAttribute('checked'))
            {
                this.kitsColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.logisticsCenterColumn.getAttribute('checked'))
            {
                this.logisticsCenterColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            if (await this.customerIdColumn.getAttribute('checked'))
            {
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
            else {
                this.customerIdColumn.click();
                browser.manage().timeouts().implicitlyWait(Vconst.swaitTime);
            }
        }
    } 
    findCurrentCustomer(){
        this.selectedCustomer.getWebElements
    }       
}